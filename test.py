def isclose(a, b):
    if (isinstance(a, float) or isinstance(a, int)) and \
       (isinstance(b, float) or isinstance(b, int)):
        return abs(a - b) < 1e-6
    else:
        return False


def run_tests(fn, inputs, outputs):
    print(fn)
    failure = False
    for (input, output) in zip(inputs, outputs):
        if isinstance(input, tuple):
            actual_output = fn(*input)
        else:
            actual_output = fn(input)
        if ( isinstance(output, float) and not isclose(output, actual_output) and \
             actual_output != output ):
            print('!!! FAILED !!!')
            print('Input:', input)
            print('Expected output:', output)
            print('Actual output:', actual_output)
            print()
            failure = True
    if not failure:
        print('All tests passed!')
        print()
