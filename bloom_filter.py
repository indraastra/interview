import math
import random
import sys

from bitarray import bitarray
import mmh3


###
# NOTE: This technique of generating n hash functions from 1 was taken from
#   http://willwhim.wpengine.com/2011/09/03/producing-n-hash-functions-by-hashing-only-once/
# The implementation, however, is my own.
# The general idea is a composition of two hashing tricks:
#   1. A good n-bit hash function can be treated as two independent
#      independent n/2-bit hash functions.
#   2. From two independent hash functions, you can generate n
#      independent hash functions by combining the outputs of step 1
#      in n unique ways.
def generate_hash32_fn(i):
    def fn(input):
        # Murmur was chosen as the hash function for reasons outlined here:
        #   https://github.com/bitly/dablooms/pull/19
        hash32_upper, hash32_lower = mmh3.hash64(input)
        return ( hash32_upper + hash32_lower * i ) & 0xffffffff
    return fn

def generate_hash32_fns(num_fns):
    hash_fns = [ generate_hash32_fn(i) for i in range(num_fns) ]
    return hash_fns
#
###


class BloomFilterBase:
    """
    Provides functionality common to all Bloom Filter implementations
    in this file.
    """
    def __init__(self, num_fns, num_buckets):
        self.num_fns = num_fns
        # NOTE: See note below about hash uniformity.
        self.num_buckets = num_buckets
        self.num_insertions = 0

        self.hash_fns = generate_hash32_fns(num_fns)

    def prob_false_positive(self, n=None):
        k = self.num_fns
        m = self.num_buckets
        n = n or self.num_insertions
        return (1 - (1 - (1 / m)) ** (k*n)) ** k

    def _bitvector_indices(self, key):
        for hash_fn in self.hash_fns:
            # TODO: Modulo here may only preserve uniformity if num_buckets
            # is a power of two. How can this be ensured?
            yield hash_fn(key) % self.num_buckets


class BloomFilterNaive(BloomFilterBase):
    """
    Uses Python's builtin arbitrary precision integers to represent the
    bitvector.
    """
    def __init__(self, num_fns, num_buckets):
        super().__init__(num_fns, num_buckets)

        self.bitvector = 0

    def __contains__(self, key):
        # This key is contained if and only if all of the buckets contain a 1 (nonzero value).
        return all((1 << bv_index) & self.bitvector \
                   for bv_index in self._bitvector_indices(key))

    def _bitvector_mask(self, key):
        bv_mask = 0
        for bv_index in self._bitvector_indices(key):
            bv_mask |= (1 << bv_index)
        return bv_mask

    def add(self, key):
        bv_mask = self._bitvector_mask(key)
        self.bitvector |= bv_mask
        self.num_insertions += 1


class BloomFilterOpt(BloomFilterBase):
    """
    Uses the thirdparty bitarray structure to represent the bitvector.
    """
    def __init__(self, num_fns, num_buckets):
        super().__init__(num_fns, num_buckets)

        self.bitvector = bitarray(num_buckets)
        self.bitvector.setall(0)

    def __contains__(self, key):
        # This key is contained if and only if all of the buckets contain a 1 (nonzero value).
        return all(self.bitvector[bv_index] == 1
                   for bv_index in self._bitvector_indices(key))

    def add(self, key):
        # Set all buckets this key falls into to 1.
        for bv_index in self._bitvector_indices(key):
            self.bitvector[bv_index] = 1
        self.num_insertions += 1


BloomFilter = BloomFilterOpt
