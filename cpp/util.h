#include <cstring>

namespace util {

bool is_sorted(int* array, size_t n);
void test_sorted(int* array, size_t n);
void print_array(int* array, size_t n);

}
