#include <iostream>

#include "util.h"

using namespace std;
using namespace util;


size_t partition(int* array, size_t lo, size_t hi) {
    int pivot = array[hi];
    int i = 0;
    for (int j = 0; j < hi; ++j) {
        if (array[j] < pivot) {
            if (i != j) {
                int tmp = array[j];
                array[j] = array[i];
                array[i] = tmp;
            }
            i++;
        }
    }
    array[hi] = array[i];
    array[i] = pivot;
    return i;
}

void quicksort_impl(int* array, size_t lo, size_t hi) {
    if (lo < hi) {
        size_t p = partition(array, lo, hi);
        if (p > 0) quicksort_impl(array, lo, p - 1);
        if (p < hi) quicksort_impl(array, p + 1, hi);
    }
}

void quicksort(int* array, size_t n) {
    quicksort_impl(array, 0, n - 1);
}

int main() {
    int array[] = { 9, 3, 2, 5, 1, 8, 6, 0, 7, 4 };
    print_array(array, (size_t)(sizeof(array)/sizeof(int)));
    quicksort(array,   (size_t)(sizeof(array)/sizeof(int)));
    print_array(array, (size_t)(sizeof(array)/sizeof(int)));
    test_sorted(array, (size_t)(sizeof(array)/sizeof(int)));
}
