#include <cstring>
#include <iostream>

using namespace std;

namespace util {

bool is_sorted(int* array, size_t n) {
    for (int i = 1; i < n; ++i) {
        if (array[i] < array[i - 1]) return false;
    }
    return true;
}

void print_array(int* array, size_t n) {
    for (int i = 0; i < n; ++i) {
        cout << array[i] << ' ';
    }
    cout << endl;
}

void test_sorted(int* array, size_t n) {
    if (is_sorted(array, n)) {
        cout << "Sorted!" << endl;
    } else {
        cout << "Failed to sort!" << endl;
    }
}

}
