import random
import string
import sys


def random_phone_number(l=7):
    return ''.join(random.choice(string.digits) for _ in range(l))

if __name__ == '__main__':
    with open(sys.argv[1], 'w', encoding='ascii') as output_file:
        for _ in range(int(sys.argv[2])):
            number = random_phone_number()
            output_file.write(number + '\n')
