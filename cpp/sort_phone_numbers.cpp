#include <bitset>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "util.h"

using namespace std;
using namespace util;

void merge(int* array, size_t n, size_t partition) {
    auto array_copy = new int[n];
    int i = 0;
    int left = 0;
    int right = partition;
    while (left < partition && right < n) {
        if (array[left] <= array[right]) {
            array_copy[i] = array[left];
            left++;
        } else {
            array_copy[i] = array[right];
            right++;
        }
        i++;
    }
    while (left < partition) {
        array_copy[i++] = array[left++];
    }
    while (right < n) {
        array_copy[i++] = array[right++];
    }
    for (int i = 0; i < n; ++i) array[i] = array_copy[i];
    delete array_copy;
}

void merge_sort_rec(int* array, size_t n) {
    if (n <= 1) {
        return;
    } else {
        size_t partition = n / 2;
        merge_sort_rec(array, partition);
        merge_sort_rec(array + partition, n - partition);
        merge(array, n, partition);
    }
}

void merge_sort_iter(int* array, size_t n) {
    if (n <= 1) {
        return;
    } else {
        //cout << "N: " << n << endl;
        for (size_t partition = 1; partition < n; partition *= 2) {
            size_t chunk = partition * 2;
            //cout << "Chunk size: " << chunk << endl;
            for (size_t i = 0; i < n; i += chunk) {
                // Only merge if there is data in the second partition.
                if ((i + partition) < n) {
                    size_t actul_size = min(chunk, n - i);
                    merge(array + i, actul_size, partition);
                }
            }
            //print_array(array, 16);
        }
    }
}

void run_tests() {
    int array[] = { 9, 3, 2, 5, 1, 8, 6, 7, 4 };
    print_array(array, sizeof(array)/sizeof(int));
    merge_sort_iter(array, sizeof(array)/sizeof(int));
    print_array(array, sizeof(array)/sizeof(int));
    test_sorted(array, sizeof(array)/sizeof(int));
}


void run_merge_sort(ifstream& phone_numbers, ofstream& sorted_numbers) {
    string line;
    vector<int> numbers;

    while ( getline(phone_numbers, line) ) {
        int num = stoi(line, nullptr);
        numbers.push_back(num);
    }
    cout << "Read " << numbers.size() << " phone numbers!" << endl;

    merge_sort_iter( &numbers[0], numbers.size());

    for (int n : numbers) {
        sorted_numbers << n << endl;
    }

    cout << "Done!";
}

void run_bit_sort(ifstream& phone_numbers, ofstream& sorted_numbers) {
    string line;
    bitset<10000000> numbers;

    while ( getline(phone_numbers, line) ) {
        int num = stoi(line, nullptr);
        numbers.set(num);
    }
    cout << "Read " << numbers.count() << " phone numbers!" << endl;

    for (int n = 0; n < 10000000; ++n) {
        if (numbers.test(n)) {
            sorted_numbers << n << endl;
        }
    }

    cout << "Done!";
}

int main() {
    string line;
    ifstream phone_numbers("phone_numbers_big.txt");
    ofstream sorted_numbers("sorted_phone_numbers.txt");

    if (!phone_numbers.is_open()) {
        cout << "Couldn't open <phone_numbers.txt>!" << endl;
    }
    if (!sorted_numbers.is_open()) {
        cout << "Couldn't open <sorted_phone_numbers.txt>!" << endl;
    }
    run_bit_sort(phone_numbers, sorted_numbers);
    sorted_numbers.close();

    return 0;
}
