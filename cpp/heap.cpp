#include <iostream>
#include <vector>

using namespace std;

template<typename T>
class heap {
public:
    enum HeapType {
        MAX_HEAP,
        MIN_HEAP
    };

    heap(HeapType type) : type(type) {
    }

    heap<T>& add(T item) {
        items.push_back(item);
        heap_up(items.size() - 1);
        return *this;
    }

    heap<T>& delete_root() {
        items[0] = items[items.size() - 1];
        items.pop_back();
        heap_down(0);
        return *this;
    }

    void print() {
        int level = 0;
        int items_at_level = 1;
        for (int i = 0; i < items.size(); ++i) {
            if (items_at_level == 0) {
                cout << endl;
                level++;
                items_at_level = 1 << level;
            }
            cout << items[i] << ' ';
            items_at_level--;
        }
        cout << endl;
    }

private:
    void heap_up(int c) {
        if (c == 0) return;

        int p;
        if (c & 1 == 0) {
            p = (c - 2) / 2;
        } else {
            p = (c - 1) / 2;
        }

        if (((type == MAX_HEAP) && (items[p] < items[c])) ||
            ((type == MIN_HEAP) && (items[p] > items[c]))) {
            // Swap and continue heaping up.
            T tmp = items[p];
            items[p] = items[c];
            items[c] = tmp;
            heap_up(p);
        }
    }

    void heap_down(int p) {
        if ((p * 2 + 2) > items.size()) return;

        int max_idx = items.size() - 1;
        int c_l = p * 2 + 1;
        int c_r = p * 2 + 2;
        int c = c_l;

        if (c_r < max_idx && (MAX_HEAP ? items[c_r] > items[c_l] : items[c_r] < items[c_l])) {
            c = c_r;
        }
        if (((type == MAX_HEAP) && (items[p] < items[c])) ||
            ((type == MIN_HEAP) && (items[p] > items[c]))) {
            // Swap and continue heaping up.
            T tmp = items[p];
            items[p] = items[c];
            items[c] = tmp;
            heap_down(c);
        }
    }


    vector<T> items;
    HeapType type;
};

int main() {
    cout << "Max heap:" << endl;
    heap<int> max_heap(heap<int>::MAX_HEAP);
    max_heap.add(10).add(3).add(23).add(14);
    max_heap.print();
    cout << endl;
    max_heap.delete_root().delete_root();
    max_heap.print();

    cout << endl << "Min heap:" << endl;
    heap<int> min_heap(heap<int>::MIN_HEAP);
    min_heap.add(10).add(3).add(23).add(14);
    min_heap.print();
    cout << endl;
    min_heap.delete_root().delete_root();
    min_heap.print();
    return 0;
}
