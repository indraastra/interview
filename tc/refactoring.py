import math

from test import run_tests

class Refactoring:
    def refactor(self, n):
        return self.refactor_from(n, 2)

    def refactor_from(self, n, start):
        result = 0
        for a in range(start, int(math.sqrt(n)) + 1):
            if n % a == 0:
                #print('>', a, n // a)
                result += self.refactor_from(n // a, a) + 1
        #print('!', n, '=>', result)
        return result


    def prime_sieve(self, n):
        primes = [True]*n
        for i in range(2, n):
            if primes[i]:
                yield i
                for j in range(i*2, n, i):
                    primes[j] = False


    def prime_factors(self, n):
        upper_bound = int(math.sqrt(n))
        primes = self.prime_sieve(upper_bound)
        factors = []
        for p in primes:
            while n % p == 0:
                n = n // p
                factors.append(p)
        return factors


refactor_inputs = [
    24,
    1,
    9973,
    9240,
]

refactor_outputs = [
    6,
    0,
    0,
    295,
]

if __name__ == '__main__':
    ref = Refactoring()
    run_tests(ref.refactor, refactor_inputs, refactor_outputs)
    #print(list(ref.prime_sieve(100)))
    #print(ref.prime_factors(9973))
    #print(ref.prime_factors(9240))
    #print(ref.count(12, 2))
