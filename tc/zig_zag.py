# My solution to the following TC problem -
# https://community.topcoder.com/stat?c=problem_statement&pm=1259&rd=4493

from test import run_tests

class ZigZag:
    def longestZigZag(self, sequence):
        lzz_up = []
        lzz_down = []

        for i in range(len(sequence)):
            pot_lzz_down = [lzz_down[j] + 1 for j in range(len(lzz_down))
                            if sequence[j] < sequence[i]]
            max_lzz_up  = max(pot_lzz_down, default=1)

            pot_lzz_up = [lzz_up[j] + 1 for j in range(len(lzz_up))
                          if sequence[j] > sequence[i]]
            max_lzz_down = max(pot_lzz_up, default=1)

            lzz_up.append(max_lzz_up)
            lzz_down.append(max_lzz_down)
        
        return max(max(lzz_up), max(lzz_down))


inputs = [
    [1, 7, 4, 9, 2, 5],
    [1, 17, 5, 10, 13, 15, 10, 5, 16, 8],
    [44],
    [1, 2, 3, 4, 5, 6, 7, 8, 9],
    [374, 40, 854, 203, 203, 156, 362, 279, 812, 955, 
     600, 947, 978, 46, 100, 953, 670, 862, 568, 188, 
     67, 669, 810, 704, 52, 861, 49, 640, 370, 908, 
     477, 245, 413, 109, 659, 401, 483, 308, 609, 120, 
     249, 22, 176, 279, 23, 22, 617, 462, 459, 244]
]

outputs = [
    6,
    7,
    1,
    2,
    36
]


if __name__ == '__main__':
    zz = ZigZag()
    run_tests(zz.longestZigZag, inputs, outputs)
