from test import run_tests

def max_profit_naive(L, H, S):
    max_profit_days = None
    max_profit = 0

    for i in range(len(S)):
        for j in range(i + 1, len(S)):
            profit_b, profit_s = S[i], S[j]
            if profit_s - profit_b > max_profit:
                max_profit = profit_s - profit_b
                max_profit_days = (i, j)

    return max_profit

def max_profit_dp(L, H, S):
    max_profit_so_far = 0
    lowest_price_so_far = S[0]

    for i in range(1, len(S)):
        max_profit_so_far = max(max_profit_so_far,
                                S[i] - lowest_price_so_far)
        lowest_price_so_far = min(lowest_price_so_far, S[i])

    return max_profit_so_far



inputs = [
    ([1, 2], [1.5, 2.4], [1.2, 2.1]),
    ([1, 2, 1.1, 1.2], [1.5, 2.4, 1.7, 3], [1.2, 2.1, 1.6, 2]),
    ([1, .9, 1.1, 1.2], [1.5, 2.4, 1.7, 3], [1.2, 1.1, 1.6, 3]),
    ([1, 2, 1.1, 1.2], [1.5, 2.4, 1.7, 3], [1.2, 2.1, 1.6, 2.4]),
    ([2, 1], [2.5, 1.4], [2.2, 1.1]),
]

outputs = [
    .9,
    .9,
    1.9,
    1.2,
    0.0
]


if __name__ == '__main__':
    run_tests(max_profit_naive, inputs, outputs)
    run_tests(max_profit_dp, inputs, outputs)
