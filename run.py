import random
import string
import sys

from bloom_filter import BloomFilter


def load_dict(dict_path):
    with open(dict_path, 'r') as words:
        return [ w.strip() for w in words ]


def random_word(len_low, len_high):
    word_len = random.randint(len_low, len_high)
    letters = random.sample(string.ascii_letters, word_len)
    return ''.join(letters)


def random_words(count):
    words = []
    for i in range(count):
        words.append( random_word(4, 10) )
    return words


if ( __name__ == '__main__' ):
    all_words = load_dict(sys.argv[1])
    num_words = len(all_words)
    print('Loaded {} words from dictionary.'.format(num_words))
    print()

    num_hashes = 10
    num_buckets = 128*1024*8  # 128KB.
    print('Constructing Bloom Filter with {} hash functions and {} buckets.'.format(num_hashes, num_buckets))
    bf = BloomFilter(num_hashes, num_buckets)
    print('Initial chance of false positive (should be 0): ', bf.prob_false_positive())
    print()

    # Load words into bloom filter and generate some random words to test out.
    print('Loading words into bloom filter.')
    include = all_words
    exclude = random_words(10000)
    for w in include:
        bf.add(w)
    p = bf.prob_false_positive()
    print('Chance of false positive now: ', p)
    print('Expected number of false positives: ', int(p * len(exclude)))
    print()
    
    false_negatives = [ w for w in include if w not in bf ]
    false_positives = [ w for w in exclude if w in bf ]
    print('All words inserted should be in bloom filter: ',
          len(false_negatives) == 0)
    print('False negative set (must be empty): ', false_negatives)
    print('False positive set ({} elements): '.format(len(false_positives)), false_positives)
    print()


    #while True:
    #    w = input("> ")
    #    if not w:
    #        break
    #    elif w in bf:
    #        print("Found!")
    #    else:
    #        print("Not found!")
